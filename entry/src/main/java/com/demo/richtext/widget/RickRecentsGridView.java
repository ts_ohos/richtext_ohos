
/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.widget;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.adapter.RickAdapter;
import com.demo.richtext.emoji.Emojicon;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.components.TextField;
import ohos.app.Context;

/**
 * @author Daniele Ricci
 * @author Hani Al Momani (hani.momanii@gmail.com)
 */
public class RickRecentsGridView extends RickGridView implements RickRecents {
    private final RickAdapter mAdapter;
    private final RickRecentsManager recents1;

    /**
     * 构造
     *
     * @param context   上下文
     * @param emojicons 数据
     * @param recents   接口
     * @param rickPopup 软键盘
     * @param textField 输入框
     */
    public RickRecentsGridView(Context context, Emojicon[] emojicons,
                               RickRecents recents, RickPopup rickPopup, TextField textField) {
        super(context, emojicons, recents, rickPopup, textField);
        recents1 = RickRecentsManager
                .getInstance(rootView.getContext());
        mAdapter = new RickAdapter(rootView.getContext(), recents1);
        ListContainer gridView = (ListContainer) rootView.findComponentById(ResourceTable.Id_Emoji_GridView);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(10);
        tableLayoutManager.setOrientation(ListContainer.HORIZONTAL);

        gridView.setLayoutManager(tableLayoutManager);
        gridView.setItemClickedListener((listContainer, component, i, l) -> {
            Emojicon emojicon = recents1.get(i);
            String s = emojicon.getEmoji();
            textField.insert(s);
        });
        gridView.setItemProvider(mAdapter);
        mAdapter.notifyDataChanged();
    }

    @Override
    public void addRecentEmoji(Context context, Emojicon emojicon, TextField textField) {
        recents1.push(emojicon);
        mAdapter.notifyDataChanged();

    }
}