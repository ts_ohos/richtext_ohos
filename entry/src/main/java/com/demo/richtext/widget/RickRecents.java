
/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.widget;

import com.demo.richtext.emoji.Emojicon;
import ohos.agp.components.TextField;
import ohos.app.Context;

/**
 * @author Daniele Ricci
 */

public interface RickRecents {
    /**
     * 接口
     *
     * @param context 上下文
     * @param emojicon 对象
     * @param textField 输入框
     */
    void addRecentEmoji(Context context, Emojicon emojicon, TextField textField);
}