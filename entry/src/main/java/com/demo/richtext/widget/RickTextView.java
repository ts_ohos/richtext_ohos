
/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

public class RickTextView extends Text {
    /**
     * 构造
     *
     * @param context 上下文
     */
    public RickTextView(Context context) {
        super(context);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrSet attSet
     */
    public RickTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public RickTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
