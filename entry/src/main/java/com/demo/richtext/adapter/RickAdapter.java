
/*
 * Copyright 2016 Hani Al Momani
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.adapter;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.emoji.Emojicon;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * @author Hani Al Momani (hani.momanii@gmail.com)
 */

public class RickAdapter extends BaseItemProvider {
    private final Context context;
    private List<Emojicon> emoticonList;
    private Emojicon[] emoticons;

    /**
     * 构造
     *
     * @param context 上下文
     * @param data 数据
     */
    public RickAdapter(Context context, List<Emojicon> data) {
        this.emoticonList = data;
        this.context = context;
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @param data 数据
     */
    RickAdapter(Context context, Emojicon[] data) {
        this.emoticons = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        if (emoticonList != null) {
            return emoticonList.size();
        } else if (emoticons != null) {
            return emoticons.length;
        } else {
            return 0;
        }
    }

    @Override
    public Emojicon getItem(int i) {
        if (emoticonList != null) {
            return emoticonList.get(i);
        } else if (emoticons != null) {
            return emoticons[i];
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component component1 = component;
        if (component1 == null) {
            component1 = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_emojicon_item, null, false);
            ViewHolder holder = new ViewHolder();
            holder.icon = (Text) component1.findComponentById(ResourceTable.Id_emojicon_icon);

            component1.setTag(holder);
        }

        Emojicon emoji = getItem(i);
        ViewHolder holder = (ViewHolder) component1.getTag();
        holder.icon.setText(emoji.getEmoji());
        return component1;
    }

    static class ViewHolder {
       private Text icon;
    }
}