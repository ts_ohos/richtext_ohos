/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.richtext.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.app.Context;

/**
 * RichTextFactory
 */
public class RichTextFactory {
    private static final int COLOR_R_CLICKABLE_TEXT = 0;
    private static final int COLOR_G_CLICKABLE_TEXT = 125;
    private static final int COLOR_B_CLICKABLE_TEXT = 255;
    private static final int FP_TEXT_SIZE = 18;
    private RichTextBuilder richTextBuilder;
    private TextForm textFormClickable;
    private TextForm textFormNormal;

    /**
     * RichTextFactory Constructor
     *
     * @param context context
     */
    public RichTextFactory(Context context) {
        this.richTextBuilder = new RichTextBuilder();
        this.textFormClickable = new TextForm();
        this.textFormNormal = new TextForm();
        RgbColor colorBlue = new RgbColor(COLOR_R_CLICKABLE_TEXT, COLOR_G_CLICKABLE_TEXT, COLOR_B_CLICKABLE_TEXT);
        RgbColor colorRed = new RgbColor(255, 0, 0);
        RgbColor colorWhite = new RgbColor(255, 255, 255);
        RgbColor colorYellow = new RgbColor(255, 255, 0);

        textFormClickable.setTextColor(colorYellow.asArgbInt());
        textFormClickable.setTextSize(AttrHelper.fp2px(FP_TEXT_SIZE, context));
        textFormClickable.setTextFont(Font.DEFAULT_BOLD);
        textFormNormal.setTextSize(AttrHelper.fp2px(FP_TEXT_SIZE, context));
        textFormNormal.setTextColor(colorWhite.asArgbInt());

//        textFormNormal.setTextColor(ResourceTable.Color_grey_background);
    }

    /**
     * Add default clickable style text to RichTextBuilder
     *
     * @param text text string
     */
    public void addClickableText(String text) {
        richTextBuilder.mergeForm(textFormClickable);
        richTextBuilder.addText(text);
        richTextBuilder.revertForm();
    }


    /**
     * Add normal style text to RichTextBuilder
     *
     * @param text text string
     */
    public void addNormalText(String text) {
        richTextBuilder.mergeForm(textFormNormal);
        richTextBuilder.addText(text);
        richTextBuilder.revertForm();
    }

    /**
     * Reset RichTextBuilder
     */
    public void clean() {
        richTextBuilder = new RichTextBuilder();
    }

    /**
     * Return a rich text object based on the latest configurations
     *
     * @return RichText object
     */
    public RichText getRichText() {
        return richTextBuilder.build();
    }

}
