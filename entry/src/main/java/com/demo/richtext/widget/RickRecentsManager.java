
/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.widget;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.demo.richtext.emoji.Emojicon;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * @author Daniele Ricci
 * @author Hani Al Momani (hani.momanii@gmail.com)
 */

public class RickRecentsManager extends ArrayList<Emojicon> {
    private static final String PREFERENCE_NAME = "emojicon";
    private static final String PREF_RECENTS = "recent_emojis";
    private static final String PREF_PAGE = "recent_page";

    private static final Object LOCK = new Object();
    private static RickRecentsManager sInstance;

    private final Context mContext;

    private RickRecentsManager(Context context) {
        mContext = context.getApplicationContext();
        loadRecents();
    }

    /**
     * 构造
     *
     * @param context 上下文
     * @return 对象
     */
    public static RickRecentsManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = new RickRecentsManager(context);
                }
            }
        }
        return sInstance;
    }

    public int getRecentPage() {
        return getPreferences().getInt(PREF_PAGE, 0);
    }

    /**
     * 页码
     *
     * @param page 页数
     */
    public void setRecentPage(int page) {
        Preferences preferences = getPreferences();
        preferences.putInt(PREF_PAGE, page);
        preferences.flushSync();
    }

    /**
     * 提交
     *
     * @param object 对象
     */
    public void push(Emojicon object) {
        if (contains(object)) {
            super.remove(object);
        }
        add(0, object);
    }

    @Override
    public boolean add(Emojicon object) {
        return super.add(object);
    }

    @Override
    public void add(int index, Emojicon object) {
        super.add(index, object);
    }

    @Override
    public boolean remove(Object object) {
        return super.remove(object);
    }

    private Preferences getPreferences() {
        return new DatabaseHelper(mContext).getPreferences(PREFERENCE_NAME);
    }

    private void loadRecents() {
        Preferences preferences = getPreferences();
        String string = preferences.getString(PREF_RECENTS, "");
        StringTokenizer tokenizer = new StringTokenizer(string, "~");
        while (tokenizer.hasMoreTokens()) {
            try {
                add(new Emojicon(tokenizer.nextToken()));
            } catch (NumberFormatException e) {
                // ignored
            }
        }
    }

    /**
     * 保存
     */
    public void saveRecents() {
        StringBuilder str = new StringBuilder();
        int size = size();
        for (int index = 0; index < size; index++) {
            Emojicon emojicon = get(index);
            str.append(emojicon.getEmoji());
            if (index < (size - 1)) {
                str.append('~');
            }
        }
        Preferences prefs = getPreferences();
        prefs.putString(PREF_RECENTS, str.toString()).flush();
    }
}
