package com.demo.richtext.slice;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.bean.RickActions;
import com.demo.richtext.bean.TopicModel;
import com.demo.richtext.bean.UserModel;
import com.demo.richtext.utils.LogUtils;
import com.demo.richtext.widget.RickEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.TextField;
import ohos.agp.window.service.DisplayManager;

/**
 * @author TS
 */
public class MainAbilitySlice extends AbilitySlice {
    private final static String TAG = "LogMainAbilitySlice";

    public final static int REQUEST_USER_CODE_INPUT = 1111;
    public final static int REQUEST_USER_CODE_CLICK = 2222;
    public final static int REQUEST_TOPIC_CODE_INPUT = 3333;
    public final static int REQUEST_TOPIC_CODE_CLICK = 4444;
    private final static int SLIDER_END = 2;

    private RickEditText richOne;
    private TextField richTwo;
    private TextField richThree;
    private DirectionalLayout tempC;
    private DirectionalLayout layout;
    private Image image1;
    private Image image2;
    private Image image3;
    private int height;
    private int width;
    private boolean isUser = false;
    private int emojiClick = 0;
    private RickActions rickActions;
    private NewEmojiSlice emojiSlice;
    private MVVMSlice mvvmSlice;

    private String insertContent = "这是测试文本#话题话题#哟 www.baidu.com " +
            " 来@某个人  @22222 @kkk " +
            " 好的,来几个表情[e2][e4][e55]，最后来一个电话 13245685478";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        closeO();
        initView();
        initClick();
    }

    private void closeO() {
        richOne = (RickEditText) findComponentById(ResourceTable.Id_emoji_edit_text);
        richTwo = (TextField) findComponentById(ResourceTable.Id_rich_text_1);
        richThree = (TextField) findComponentById(ResourceTable.Id_rich_text_2);
        tempC = (DirectionalLayout) findComponentById(ResourceTable.Id_c);
        emojiSlice = new NewEmojiSlice();
        mvvmSlice = new MVVMSlice();
        richThree.setBubbleSize(0, 0);
    }

    private void initView() {
        richTwo.setText(insertContent);
        richTwo.setClickable(false);
        richThree.setText(insertContent);
        richThree.setClickable(true);
        width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        height = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().height;
        rickActions = new RickActions(this, tempC, width, 850, richOne);
        rickActions.setEmojiButtonPixmap(ResourceTable.Media_ic_action_keyboard, ResourceTable.Media_smiley);
    }


    private void initClick() {
        findComponentById(ResourceTable.Id_emoji_show_at)
                .setClickedListener(component ->
                        presentForResult(new UserListSlice(), new Intent(),
                                MainAbilitySlice.REQUEST_USER_CODE_CLICK));

        findComponentById(ResourceTable.Id_emoji_show_topic)
                .setClickedListener(component ->
                        presentForResult(new TopicListSlice(), new Intent(),
                                MainAbilitySlice.REQUEST_TOPIC_CODE_CLICK));

        findComponentById(ResourceTable.Id_insert_text_btn).setClickedListener(component ->
                richOne.setText(insertContent)
        );

        findComponentById(ResourceTable.Id_jump_btn).setClickedListener(component ->
                present(emojiSlice, new Intent()));

        findComponentById(ResourceTable.Id_jump_mvvm).setClickedListener(component ->
                present(mvvmSlice, new Intent()));

        findComponentById(ResourceTable.Id_emoji_show_bottom).setClickedListener(component -> {
            if (isUser) {
                hide();
            } else {
                show();
            }
        });
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        try {
            if (resultIntent == null) {
                return;
            }
            super.onResult(requestCode, resultIntent);
            switch (requestCode) {
                case MainAbilitySlice.REQUEST_USER_CODE_CLICK:
                    UserModel userModel = resultIntent.getSerializableParam("data");
                    richOne.append(userModel.getUserName());
                    break;
                case MainAbilitySlice.REQUEST_TOPIC_CODE_CLICK:
                    TopicModel topicModel = resultIntent.getSerializableParam("data");
                    richOne.append(topicModel.getTopicName());
                default:
                    break;
            }
        } catch (Exception e) {
            LogUtils.e(TAG, e.toString());
        }
    }

    private void show() {
        rickActions.showPopup();
        isUser = true;
    }

    private void hide() {
        rickActions.hidePopup();
        isUser = false;
    }

    @Override
    public void onActive() {

        super.onActive();
    }

    @Override
    protected void onBackground() {
        hide();
        super.onBackground();
    }

    @Override
    public void onForeground(Intent intent) {

        super.onForeground(intent);
    }
}
