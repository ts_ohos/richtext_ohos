
package com.demo.richtext.slice;

import com.demo.richtext.ResourceTable;
import com.xw.repo.XEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * Main ability slice
 */
public class MyTestAbilitySlice extends AbilitySlice {
    private XEditText clearXEdit;
    private XEditText customXEdit;
    private XEditText showXEdit;
    private XEditText clear_edit_text, displayText, defaultText;
    private Text textView1;
    private Text textView2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_my_test_ability_main);
        Button showPatternBtn = (Button) findComponentById(ResourceTable.Id_show_pattern_btn);
        clearXEdit = (XEditText) findComponentById(ResourceTable.Id_clear_marker_edit_text);
        customXEdit = (XEditText) findComponentById(ResourceTable.Id_custom_edit_text);
        clear_edit_text = (XEditText) findComponentById(ResourceTable.Id_clear_edit_text);
        showXEdit = (XEditText) findComponentById(ResourceTable.Id_show_separator_edit_text);
        textView1 = (Text) findComponentById(ResourceTable.Id_text1);
        textView2 = (Text) findComponentById(ResourceTable.Id_text2);
        displayText = (XEditText) findComponentById(ResourceTable.Id_display_text);
        defaultText = (XEditText) findComponentById(ResourceTable.Id_default_edit_text);

        displayText.setClickable(false);
        defaultText.setOnXTextChangeListener(new XEditText.OnXTextChangeListener() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                displayText.setText(defaultText.getText());
            }

            @Override
            public void afterTextChanged(String string) {
                displayText.setText(defaultText.getText());
            }
        });


        clearXEdit.setOnXTextChangeListener(new XEditText.OnXTextChangeListener() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(String string) {
                textView1.setText(clearXEdit.getTextTrimmed());
            }
        });

        customXEdit.setOnXTextChangeListener(new XEditText.OnXTextChangeListener() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(String string) {
                textView2.setText(customXEdit.getTextTrimmed());
            }
        });
        showXEdit.setSeparator(" ");
        showXEdit.setPattern(new int[]{3, 4, 4});
        showPatternBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showXEdit.setTextEx("13800000000");
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
