/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.richtext.slice;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.adapter.PageProvider;
import com.demo.richtext.bean.RickActions;
import com.demo.richtext.widget.RickEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.DisplayManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TS
 */
public class MVVMSlice extends AbilitySlice {

    private int textFlag = 0;
    private String content;
    private String title;
    private DirectionalLayout tempC;
    private Text mTitle;
    private RickEditText input;
    private Button emojiBtn;
    private DirectionalLayout directionalLayout;

    private Image image1;
    private Image image2;
    private Image image3;

    private PageSlider pageSlider;


    private int height;
    private int width;
    private boolean isUser = false;
    private RickActions rickActions;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_mvvm);
        close();
        initView();
        startView();
        initPages();
    }

    private void close() {
        input = (RickEditText) findComponentById(ResourceTable.Id_mvvm_input_text);
        input.setBubbleSize(0, 0);
    }

    private void initView() {
        mTitle = (Text) findComponentById(ResourceTable.Id_mvvm_title);
        emojiBtn = (Button) findComponentById(ResourceTable.Id_mvvm_emoji_btn);
        directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_emoji_mvvm);
        image1 = (Image) findComponentById(ResourceTable.Id_focus_one1);
        image2 = (Image) findComponentById(ResourceTable.Id_focus_two1);
        image3 = (Image) findComponentById(ResourceTable.Id_focus_three1);
        tempC = (DirectionalLayout) findComponentById(ResourceTable.Id_c);
        width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        height = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().height;
        rickActions = new RickActions(this, tempC, width, 850, input);
        rickActions.setEmojiButtonPixmap(ResourceTable.Media_ic_action_keyboard, ResourceTable.Media_smiley);
        findComponentById(ResourceTable.Id_mvvm_change_text).setClickedListener(component ->
                changeClick());

        findComponentById(ResourceTable.Id_mvvm_input_btn).setClickedListener(component -> {
            emojiBtn.setVisibility(Component.VISIBLE);
            input.setText("");
            input.setClickable(true);
            input.setHint("请输入帖子内容");
            mTitle.setText("编辑框模式");
        });

        emojiBtn.setClickedListener(component -> {
            if (isUser) {
                hide();
            } else {
                show();
            }
        });
    }

    private void show() {
        rickActions.showPopup();
        isUser = true;
    }

    private void hide() {
        rickActions.hidePopup();
        isUser = false;
    }

    private void initPages() {
        Component component1 = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_emoji, null, false);
        Component component2 = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_emoji2, null, false);
        Component component3 = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_emoji3, null, false);
        List<Component> viewList = new ArrayList<>();
        viewList.add(component1);
        viewList.add(component2);
        viewList.add(component3);
        Component component = findComponentById(ResourceTable.Id_emoji_slider_mvvm);
        if (component == null) {
            return;
        }
        pageSlider = (PageSlider) component;
        pageSlider.setProvider(new PageProvider(this, viewList));
        pageSlider.setOrientation(Component.HORIZONTAL);
        pageSlider.setSlidingPossible(true);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {
                if (i == 2) {
                    switch (pageSlider.getCurrentPage()) {
                        case 0:
                            image1.setPixelMap(ResourceTable.Media_page_indicator_focused);
                            image2.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            image3.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            break;
                        case 1:
                            image1.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            image2.setPixelMap(ResourceTable.Media_page_indicator_focused);
                            image3.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            break;
                        case 2:
                            image1.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            image2.setPixelMap(ResourceTable.Media_page_indicator_unfocused);
                            image3.setPixelMap(ResourceTable.Media_page_indicator_focused);
                            break;
                        default:
                            break;
                    }
                }
            }

            @Override
            public void onPageChosen(int i) {

            }
        });
    }


    private void startView() {
        input.setText("");
        input.setClickable(false);
        mTitle.setText("未输入文本");
    }

    private void changeClick() {
        input.setClickable(false);
        input.setBubbleSize(0, 0);
        switch (textFlag) {
            case 0:
                textFlag = 1;
                content = "这是测试#话题话题#文本哟 www.baidu.com " +
                        "\n来@某个人  @22222 @kkk " +
                        "\n好的,来几个表情[e2][e4][e55]，最后来一个电话 13245685478";
                title = "多种数据类型";
                mTitle.setText(title);
                input.setText(content);
                break;
            case 1:
                textFlag = 2;
                content = "这是普通的测试文本";
                title = "普通文本类型";
                mTitle.setText(title);
                input.setText(content);
                break;
            case 2:
                textFlag = 3;
                content = "这是只有表情[e2][e4][e55]";
                title = "标签文本类型";
                mTitle.setText(title);
                input.setText(content);
                break;
            case 3:
                textFlag = 0;
                content = "这是测试@人的文本 " +
                        "\n来@kkk  @22222 ";
                title = "@人文本类型";
                mTitle.setText(title);
                input.setText(content);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onBackground() {
        hide();
        super.onBackground();
    }

    @Override
    protected void onActive() {
        super.onActive();
    }
}
