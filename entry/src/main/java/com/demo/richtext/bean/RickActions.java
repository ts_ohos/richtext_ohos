
/*
 * Copyright 2016 Hani Al Momani
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.bean;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.widget.RickPopup;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.TextField;
import ohos.app.Context;

/**
 * RickActions
 *
 * @author Hani Al Momani (hani.momanii@gmail.com)
 */
public class RickActions implements Component.FocusChangedListener {
    private RickPopup popup;
    private int showPixmap;
    private int hidePixmap;
    private TextField TextField;

    private int keyBoardIcon = ResourceTable.Media_ic_action_keyboard;
    private int smileyIcons = ResourceTable.Media_smiley;


    /**
     * Constructor
     *
     * @param ctx       The context of current activity.
     * @param rootView  The top most layout in your view hierarchy. The difference of this view and the screen height will be used to calculate the keyboard height.
     * @param width     宽
     * @param height    高
     * @param textField 输入框
     */
    public RickActions(Context ctx, DirectionalLayout rootView, int width, int height, TextField textField) {
        this.popup = new RickPopup(ctx, width, height, textField);
        this.TextField = textField;
        rootView.addComponent(popup);
        setCursorVisible(textField);
    }

    private void setCursorVisible(TextField textField) {
        textField.setFocusable(Component.FOCUS_ENABLE);
        textField.setEnabled(true);
        textField.requestFocus();
        textField.setTextCursorVisible(true);
    }

    public RickPopup getPopup() {
        return popup;
    }


    /**
     * 设图片
     *
     * @param showpixmap 显示
     * @param hidepixmap 隐藏
     * @noinspection checkstyle:JavadocMethod
     */
    public void setEmojiButtonPixmap(int showpixmap, int hidepixmap) {
        this.showPixmap = showpixmap;
        this.hidePixmap = hidepixmap;
    }


    private void togglePopupVisibility() {
        if (popup.getVisibility() == Component.HIDE) {
            showPopup();
        } else {
            hidePopup();
        }
        setCursorVisible(TextField);
    }

    /**
     * 显示
     */
    public void showPopup() {
        if (popup != null && popup.getVisibility() == Component.HIDE) {
            popup.show();
        }
        visiblePopup();
    }

    /**
     * 隐藏
     */
    public void hidePopup() {
        if (popup != null && popup.getVisibility() == Component.VISIBLE) {
            popup.hide();
        }
        verticalPopup();
    }

    /**
     * 隐藏
     */
    public void verticalPopup() {
        if (popup != null && popup.getVisibility() == Component.VISIBLE) {
            popup.setVisibility(Component.VERTICAL);
        }
    }

    /**
     * 显示
     */
    public void visiblePopup() {
        if (popup != null && popup.getVisibility() == Component.VERTICAL) {
            popup.setVisibility(Component.VISIBLE);
        }
    }


    public int getVisibility() {
        return popup.getVisibility();
    }

    /**
     * 设置颜色
     *
     * @param tabsColor       导航
     * @param backgroundColor 背景色
     */
    public void setColors(int tabsColor, int backgroundColor) {
        this.popup.setColors(tabsColor, backgroundColor);
    }

    /**
     * 设置id
     *
     * @param keyboardIcon 键盘
     * @param smileyIcon
     */
    public void setIconsIds(int keyboardIcon, int smileyIcon) {
        this.keyBoardIcon = keyboardIcon;
        this.smileyIcons = smileyIcon;
    }


    @Override
    public void onFocusChange(Component component, boolean hasFocus) {
        if (hasFocus) {
            if (component instanceof TextField) {
                TextField = (TextField) component;
            }
        }
    }

    private boolean isUseSystemDefault;

    /**
     * 使用系统
     *
     * @param useSystemEmoji 标记
     */
    public void setUseSystemEmoji(boolean useSystemEmoji) {
        setUseSystemDefault(useSystemEmoji);
    }

    public void setUseSystemDefault(boolean isSystemDefault) {
        isUseSystemDefault = isSystemDefault;
    }
}
