package com.demo.richtext.slice;

import com.bilibili.draweetext.DraweeSpan;
import com.bilibili.draweetext.DraweeTextView;
import com.demo.richtext.ResourceTable;
import com.demo.richtext.bean.RickActions;
import com.demo.richtext.bean.TopicModel;
import com.demo.richtext.bean.UserModel;
import com.demo.richtext.utils.RichTextFactory;
import com.xw.repo.LogUtil;
import com.xw.repo.XEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.util.Optional;

import static com.bilibili.draweetext.util.Constants.HEIGHT;
import static com.bilibili.draweetext.util.Constants.WIDTH;
import static com.xw.repo.ResUtil.getPixelMap;

public class MyMainAbilitySlice extends AbilitySlice {
    private XEditText inputText;
    private Text displayText, addTopicButton;
    private boolean isUser = false;
    private Button insertTextButton, insertPicture, jumpButton;
    private Image emojiShowButton, addUserButton;
    private DraweeTextView displayDraweeTextView;
    private DirectionalLayout layoutContainer;
    public final static int REQUEST_USER_CODE_INPUT = 1111;
    public final static int REQUEST_USER_CODE_CLICK = 2222;
    public final static int REQUEST_TOPIC_CODE_INPUT = 3333;
    public final static int REQUEST_TOPIC_CODE_CLICK = 4444;
    private final static int SLIDER_END = 2;

    private RgbColor colorBlue = new RgbColor(0, 125, 255);
    private RgbColor colorRed = new RgbColor(255, 0, 0);
    private RgbColor colorWhite = new RgbColor(255, 255, 255);
    private RgbColor colorBlack = new RgbColor(255, 255, 255);
    private RgbColor colorYellow = new RgbColor(255, 255, 0);
    private String insertContent = "这是测试文本#话题话题#哟 www.baidu.com " +
            " 来@某个人  @22222 @kkk " +
            " 好的,来几个表情" + "\uD83C\uDF4B\uD83C\uDF4A\uD83C\uDF50\uD83C\uDF4F" + "最后来一个电话 13245685478";

    RichText richText = new RichTextBuilder()
            .mergeForm(new TextForm().setUnderline(true).setTextColor(colorWhite.asArgbInt())).addText("这是测试文本: ")
            .revertForm().mergeForm(new TextForm().setSubscript(true).setTextColor(colorYellow.asArgbInt())).addText("#话题话题#")
            .revertForm().mergeForm(new TextForm().setTextColor(colorWhite.asArgbInt())).addText(" , @某个人 ")
            .revertForm().mergeForm(new TextForm().setTextColor(colorRed.asArgbInt())).addText("@22222 @kkkk")
            .revertForm().addText("\n").mergeForm(new TextForm().setStrikethrough(true).setTextColor(colorWhite.asArgbInt())).addText("\n添加电话号码：186 0000 1111")
            .build();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_my_ability_main);
        initView();
        initRichText();
        initPictures();
    }

    private DirectionalLayout tempC;
    private int height;
    private int width;
    private XEditText richOne;

    private void initRichText() {
        RichTextFactory richTextFactory = new RichTextFactory(getContext());
        richTextFactory.addClickableText("@某某某");
        richTextFactory.addNormalText(" @33333");
        RichText openSourceText = richTextFactory.getRichText();
        DraweeTextView openSourceTextContainer = (DraweeTextView) findComponentById(ResourceTable.Id_openSourceNoticeText);
        openSourceTextContainer.setRichText(openSourceText);

        openSourceTextContainer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });

        richTextFactory.clean();

        RichTextFactory addRichTextFactory = new RichTextFactory(getContext());
        addRichTextFactory.addNormalText("添加链接：");
        addRichTextFactory.addClickableText("www.baidu.com");

        addRichTextFactory.addNormalText(" 添加人物：");
        addRichTextFactory.addClickableText("@22222 ");

        addRichTextFactory.addNormalText("添加话题：");
        addRichTextFactory.addClickableText("@啦啦啦啦啦");

        RichText protocolPrivacyText = addRichTextFactory.getRichText();
        DraweeTextView protocolPrivacyTextContainer = (DraweeTextView) findComponentById(ResourceTable.Id_protocolPrivacyText);
        protocolPrivacyTextContainer.setRichText(protocolPrivacyText);

        protocolPrivacyTextContainer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
    }

    public void initView() {
        richOne = (XEditText) findComponentById(ResourceTable.Id_default_edit_text);
        tempC = (DirectionalLayout) findComponentById(ResourceTable.Id_emoji_layout);
        width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        height = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().height;
        rickActions = new RickActions(this, tempC, width, 850, richOne);

        layoutContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_layout_container);
        inputText = (XEditText) findComponentById(ResourceTable.Id_default_edit_text);
        displayText = (Text) findComponentById(ResourceTable.Id_display_normal_text);
        insertTextButton = (Button) findComponentById(ResourceTable.Id_insert_text_btn);
        insertPicture = (Button) findComponentById(ResourceTable.Id_insert_picture);
        addUserButton = (Image) findComponentById(ResourceTable.Id_emoji_show_at);
        addTopicButton = (Text) findComponentById(ResourceTable.Id_emoji_show_topic);
        jumpButton = (Button) findComponentById(ResourceTable.Id_jump_mvvm);
        emojiShowButton = (Image) findComponentById(ResourceTable.Id_emoji_show_button);
        displayDraweeTextView = (DraweeTextView) findComponentById(ResourceTable.Id_display_drawee_text);

        displayText.setRichText(richText);
        displayDraweeTextView.setRichText(richText);

        insertPicture.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                layoutContainer.setVisibility(Component.VISIBLE);
            }
        });

        insertTextButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
//                displayText.setText(insertContent);
                inputText.setText(insertContent);
//                displayDraweeTextView.setText(inputText.getTextTrimmed());

                displayText.setRichText(richText);
//                inputText.setRichText(richText);
                displayDraweeTextView.setRichText(richText);

                PixelMapElement pixelMapElement = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_e1);
                DraweeSpan span = new DraweeSpan.Builder(
                        "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                        .setPlaceHolderImage(pixelMapElement)
                        .build();

                PixelMapElement pixelMapElement2 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_icon);
                DraweeSpan span2 = new DraweeSpan.Builder(
                        "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                        .setPlaceHolderImage(pixelMapElement2)
                        .build();

//                displayDraweeTextView.setText("--");
                displayDraweeTextView.setTextColor(Color.RED);
                displayDraweeTextView.setText(insertContent);

            }
        });

        emojiShowButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isUser) {
                    hide();
                } else {
                    show();
                }
            }
        });

        addUserButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                presentForResult(new UserListSlice(), new Intent(),
                        MainAbilitySlice.REQUEST_USER_CODE_CLICK);
            }
        });

        addTopicButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                presentForResult(new TopicListSlice(), new Intent(),
                        MainAbilitySlice.REQUEST_TOPIC_CODE_CLICK);
            }
        });

        jumpButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new MyTestAbilitySlice(), new Intent());
            }
        });

        inputText.setOnXTextChangeListener(new XEditText.OnXTextChangeListener() {
            @Override
            public void beforeTextChanged(CharSequence CharSequences, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence CharSequences, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(String string) {
                displayText.setText(inputText.getTextTrimmed());
                System.out.println("===TEST===: " + inputText.getTextTrimmed());
                displayDraweeTextView.setText(inputText.getTextTrimmed());

            }
        });
    }

    public void initPictures() {
        PixelMapElement pixelMapElement = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_e1);
        DraweeSpan span = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement)
                .build();

        PixelMapElement pixelMapElement2 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_e2);
        DraweeSpan span2 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement2)
                .build();

        PixelMapElement pixelMapElement3 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_333);
        DraweeSpan span3 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement3)
                .build();

        PixelMapElement pixelMapElement4 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_444);
        DraweeSpan span4 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement4)
                .build();

        PixelMapElement pixelMapElement5 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_555);
        DraweeSpan span5 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement5)
                .build();

        PixelMapElement pixelMapElement6 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_666);
        DraweeSpan span6 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement6)
                .build();

        DraweeTextView draweeTextView1 = new DraweeTextView(MyMainAbilitySlice.this);
//                draweeTextView1.setText("O_O");
        draweeTextView1.setTextSize(50);
        draweeTextView1.setAroundElements(span, null, span2, null);
        draweeTextView1.setTextColor(Color.WHITE);
        draweeTextView1.setPadding(8, 8, 8, 8);

        DraweeTextView draweeTextView2 = new DraweeTextView(MyMainAbilitySlice.this);
        draweeTextView2.setText("~~~~插入右侧图片");
        draweeTextView2.setTextSize(50);
        draweeTextView2.setAroundElements(null, null, span3, null);
        draweeTextView2.setTextColor(Color.RED);
        draweeTextView2.setPadding(8, 8, 8, 8);

        DraweeTextView draweeTextView3 = new DraweeTextView(MyMainAbilitySlice.this);
        draweeTextView3.setText("插入左侧图片~~~~");
        draweeTextView3.setTextSize(50);
        draweeTextView3.setMultipleLine(true);
        draweeTextView3.setAroundElements(span4, null, null, null);
        draweeTextView3.setTextColor(Color.CYAN);
        draweeTextView3.setPadding(8, 8, 8, 8);

        DraweeTextView draweeTextView4 = new DraweeTextView(MyMainAbilitySlice.this);
        draweeTextView4.setText("~~~~插入两侧图片~~~~");
        draweeTextView4.setTextSize(50);
        draweeTextView4.setMultipleLine(true);
        draweeTextView4.setAroundElements(span5, null, span6, null);
        draweeTextView4.setTextColor(Color.GREEN);
        draweeTextView4.setPadding(8, 8, 8, 8);

        layoutContainer.setOrientation(Component.VERTICAL);
        layoutContainer.setPadding(8, 8, 8, 8);
        layoutContainer.removeAllComponents();

        layoutContainer.addComponent(draweeTextView1);
        layoutContainer.addComponent(draweeTextView2);
        layoutContainer.addComponent(draweeTextView3);
        layoutContainer.addComponent(draweeTextView4);
    }

    private RickActions rickActions;

    private void show() {
        rickActions.showPopup();
        isUser = true;
    }

    private void hide() {
        rickActions.hidePopup();
        isUser = false;
    }

    public static PixelMapElement getPixelMapDrawable(Context context, int resId) {
        Optional<PixelMap> optional = getPixelMap(context, resId);
        return optional.map(PixelMapElement::new).orElse(null);
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {

        try {
            if (resultIntent == null) {
                return;
            }
            super.onResult(requestCode, resultIntent);
            switch (requestCode) {
                case MainAbilitySlice.REQUEST_USER_CODE_CLICK:
                    UserModel userModel = resultIntent.getSerializableParam("data");
                    richOne.append(userModel.getUserName());
                    break;

                case MainAbilitySlice.REQUEST_TOPIC_CODE_CLICK:
                    TopicModel topicModel = resultIntent.getSerializableParam("data");
                    richOne.append(topicModel.getTopicName());
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            LogUtil.error("TAG", e.toString());
        }
    }
}
