
/*
 * Copyright 2014 Hieu Rocker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.richtext.widget;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.adapter.RickAdapter;
import com.demo.richtext.emoji.Emojicon;
import com.demo.richtext.emoji.People;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Arrays;

public class RickGridView {
    public Component rootView;
    private final Emojicon[] mData;
    private RickRecents mRecents;

    /**
     * 构造
     *
     * @param context 上下文
     * @param emojicons 数据
     * @param recents 接口
     * @param rickPopup 软键盘
     * @param textField 输入框
     */
    public RickGridView(Context context, Emojicon[] emojicons, RickRecents recents, RickPopup rickPopup, TextField textField) {
        setRecents(recents);
        rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_emojicon_grid, null, false);
        ListContainer gridView = (ListContainer) rootView.findComponentById(ResourceTable.Id_Emoji_GridView);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(10);
        tableLayoutManager.setOrientation(ListContainer.HORIZONTAL);

        gridView.setLayoutManager(tableLayoutManager);

        if (emojicons == null) {
            mData = People.DATA;
        } else {
            mData = Arrays.asList((Object[]) emojicons).toArray(new Emojicon[0]);
        }
        RickAdapter mAdapter = new RickAdapter(context, Arrays.asList(mData));
        gridView.setItemProvider(mAdapter);
        gridView.setItemClickedListener((listContainer, component, i, l) -> {
            Emojicon mDatum = mData[i];
            if (mRecents != null) {
                mRecents.addRecentEmoji(rootView.getContext(), mDatum, textField);
            }
        });
    }

    private void setRecents(RickRecents recents) {
        mRecents = recents;
    }
}