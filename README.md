# GSYRichText

## 项目介绍
本项目是基于开源项目[GSYRickText](https://github.com/CarGuo/GSYRickText) 进行harmonyos化的移植和开发的。  

移植版本：v2.0.2

支持类似微博的文本效果，表情、@某人、话题、url链接等。
DEMO同时演示了不同输入框输入效果的使用。  


#### 项目名称：GSYRichText
#### 所属系列：harmonyos的第三方组件适配移植

#### 功能：一个适用于harmonyos的富文本显示框架
#### 项目移植状态：部分移植
#### 调用差异：由于缺少部分原生API支持，因此功能只实现部分
#### 原项目GitHub地址：https://github.com/CarGuo/GSYRickText

## 支持功能
* 表情
* ‘#’话题
* ‘@’某人
* url与数字（可配置）
* 点击效果
* 表情大小设置、居中显示

## 安装教程

#### 方案一  
 
```
  //核心引入 
  implementation project(':draweetext')
  implementation project(':xedittext')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
mavenCentral()
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:draweetext:1.0.0'
implementation 'com.gitee.ts_ohos:xedittext:1.0.0'
```

## 使用说明

#### 代码使用  
RichTextFactory类，用来控制富文本的样式(字体颜色，大小等)以及如何添加富文本。
````
    /**
     * RichTextFactory Constructor
     *
     * @param context context
     */
    public RichTextFactory(Context context) {
        this.richTextBuilder = new RichTextBuilder();
        this.textFormClickable = new TextForm();
        this.textFormNormal = new TextForm();
        RgbColor colorBlue = new RgbColor(COLOR_R_CLICKABLE_TEXT, COLOR_G_CLICKABLE_TEXT, COLOR_B_CLICKABLE_TEXT);
        RgbColor colorRed = new RgbColor(255, 0, 0);
        RgbColor colorWhite = new RgbColor(255, 255, 255);
        RgbColor colorYellow = new RgbColor(255, 255, 0);

        textFormClickable.setTextColor(colorYellow.asArgbInt());
        textFormClickable.setTextSize(AttrHelper.fp2px(FP_TEXT_SIZE, context));
        textFormClickable.setTextFont(Font.DEFAULT_BOLD);
        textFormNormal.setTextSize(AttrHelper.fp2px(FP_TEXT_SIZE, context));
        textFormNormal.setTextColor(colorWhite.asArgbInt());

//        textFormNormal.setTextColor(ResourceTable.Color_grey_background);
    }
... ...

    /**
     * Add default clickable style text to RichTextBuilder
     *
     * @param text text string
     */
    public void addClickableText(String text) {
        richTextBuilder.mergeForm(textFormClickable);
        richTextBuilder.addText(text);
        richTextBuilder.revertForm();
    }


    /**
     * Add normal style text to RichTextBuilder
     *
     * @param text text string
     */
    public void addNormalText(String text) {
        richTextBuilder.mergeForm(textFormNormal);
        richTextBuilder.addText(text);
        richTextBuilder.revertForm();
    }
````
普通富文本效果展示，使用自带API。
````
    RichText richText = new RichTextBuilder()
            .mergeForm(new TextForm().setUnderline(true).setTextColor(colorWhite.asArgbInt())).addText("这是测试文本: ")
            .revertForm().mergeForm(new TextForm().setSubscript(true).setTextColor(colorYellow.asArgbInt())).addText("#话题话题#")
            .revertForm().mergeForm(new TextForm().setTextColor(colorWhite.asArgbInt())).addText(" , @某个人 ")
            .revertForm().mergeForm(new TextForm().setTextColor(colorRed.asArgbInt())).addText("@22222 @kkkk")
            .revertForm().addText("\n").mergeForm(new TextForm().setStrikethrough(true).setTextColor(colorWhite.asArgbInt())).addText("\n添加电话号码：186 0000 1111")
            .build();
````

自定义富文本效果展示，点击效果，连续添加文字
````
        //添加富文本
        RichTextFactory addRichTextFactory = new RichTextFactory(getContext());
        addRichTextFactory.addNormalText("添加链接：");
        addRichTextFactory.addClickableText("www.baidu.com");

        addRichTextFactory.addNormalText(" 添加人物：");
        addRichTextFactory.addClickableText("@22222 ");

        addRichTextFactory.addNormalText("添加话题：");
        addRichTextFactory.addClickableText("@啦啦啦啦啦");

        RichText protocolPrivacyText = addRichTextFactory.getRichText();
        DraweeTextView protocolPrivacyTextContainer = (DraweeTextView) findComponentById(ResourceTable.Id_protocolPrivacyText);
        protocolPrivacyTextContainer.setRichText(protocolPrivacyText);

        //添加点击效果
        protocolPrivacyTextContainer.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
    }
````
插入文字表情图片
````
        PixelMapElement pixelMapElement = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_e1);
        DraweeSpan span = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement)
                .build();

        PixelMapElement pixelMapElement2 = getPixelMapDrawable(MyMainAbilitySlice.this, ResourceTable.Media_e2);
        DraweeSpan span2 = new DraweeSpan.Builder(
                "https://i0.hdslb.com/bfs/vip/7a4cb0b644214d476ce198ddf6a7a0aa31311199.png").setLayout(WIDTH, HEIGHT)
                .setPlaceHolderImage(pixelMapElement2)
                .build();
... ...
        DraweeTextView draweeTextView1 = new DraweeTextView(MyMainAbilitySlice.this);
        draweeTextView1.setText("O_O");
        draweeTextView1.setTextSize(50);
        draweeTextView1.setAroundElements(span, null, span2, null);
        draweeTextView1.setTextColor(Color.WHITE);
        draweeTextView1.setPadding(8, 8, 8, 8);
````

## License

MIT License

Copyright (c) 2017 Shuyu Guo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
