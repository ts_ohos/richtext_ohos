/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.richtext.slice;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.bean.RickActions;
import com.demo.richtext.widget.RickEditText;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.window.service.DisplayManager;

/**
 * @author TS
 */
public class NewEmojiSlice extends AbilitySlice {
    private int height;
    private int width;
    private boolean isUser = false;
    private RickActions rickActions;
    private RickEditText editText;
    private DirectionalLayout tempC;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_input);
        width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().width;
        height = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes().height;
        editText=(RickEditText)findComponentById(ResourceTable.Id_edit_text);
        tempC = (DirectionalLayout) findComponentById(ResourceTable.Id_c);
        rickActions = new RickActions(this, tempC, width, 850, editText);
        rickActions.setEmojiButtonPixmap(ResourceTable.Media_ic_action_keyboard, ResourceTable.Media_smiley);
        findComponentById(ResourceTable.Id_emoji_show_bottom).setClickedListener(component -> {
            if (isUser) {
                hide();
            } else {
                show();
            }
        });

    }
    private void show() {
        rickActions.showPopup();
        isUser = true;
    }

    private void hide() {
        rickActions.hidePopup();
        isUser = false;
    }
    @Override
    protected void onBackground() {
        hide();
        super.onBackground();
    }
    @Override
    protected void onActive() {
        super.onActive();
    }
}
