/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.richtext.slice;

import com.demo.richtext.ResourceTable;
import com.demo.richtext.adapter.UserListProvider;
import com.demo.richtext.bean.UserModel;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author TS
 */
public class UserListSlice extends AbilitySlice {

    public final String DATA = "data";

    private ListContainer userList;

    private List<UserModel> data = new ArrayList<>();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_user_list);

        for (int i = 0; i < 19; i++) {
            UserModel userModel = new UserModel();
            userModel.setUser_name("测试名字" + i);
            userModel.setUser_id(i * 30 + "");
            data.add(userModel);
        }

        userList = (ListContainer) findComponentById(ResourceTable.Id_user_list);
        UserListProvider provider = new UserListProvider(data, UserListSlice.this);
        userList.setItemProvider(provider);
        userList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component,
                                      int position, long id) {
                Intent intent1 = new Intent();
                intent1.setParam(DATA, data.get(position));
                setResult(intent1);
                terminate();
            }
        });
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

}
