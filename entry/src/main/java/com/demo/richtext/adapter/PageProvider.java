/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.demo.richtext.adapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.List;

/**
 * @author TS
 */
public class PageProvider extends PageSliderProvider {

    private AbilitySlice slice;

    private List<Component> components;

    public PageProvider(AbilitySlice slice, List<Component> components) {
        this.slice = slice;
        this.components = components;
    }

    @Override
    public int getCount() {
        return components.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component = null;
        if (componentContainer == null) {
            return component;
        }
        if (0 <= position && position < components.size()) {
            component = components.get(position);
        }
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer,
                                         int position, Object obj) {
        if (componentContainer == null) {
            return;
        }
        if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }
}
