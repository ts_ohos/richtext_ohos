package com.demo.richtext;

import com.demo.richtext.slice.MyMainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(MainAbilitySlice.class.getName());
//        super.setMainRoute(MyTestAbilitySlice.class.getName());
        super.setMainRoute(MyMainAbilitySlice.class.getName());

    }
}
