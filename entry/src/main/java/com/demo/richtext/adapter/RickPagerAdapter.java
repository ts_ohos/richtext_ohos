
/*
 * Copyright (C) 2021 The Chinese Software International Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.demo.richtext.adapter;

import com.demo.richtext.widget.RickRecentsGridView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import com.demo.richtext.widget.RickGridView;

import java.util.List;

public class RickPagerAdapter extends PageSliderProvider {
    private final List<RickGridView> views;


    /**
     * 构造
     *
     * @param views 子视图
     */
    public RickPagerAdapter(List<RickGridView> views) {
        super();
        this.views = views;
    }
    
    /**
     * 获取视图
     *
     * @return 视图
     */
    public RickRecentsGridView getRecentFragment() {
        for (RickGridView it : views) {
            if (it instanceof RickRecentsGridView) {
                return (RickRecentsGridView) it;
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component = views.get(position).rootView;
        componentContainer.addComponent(component, 0);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object view) {
        componentContainer.removeComponent((Component) view);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object key) {
        return key == component;
    }
}
